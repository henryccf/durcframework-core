package org.durcframework.core;

public interface IUser {

	String getUsername();
	String getPassword();
}
