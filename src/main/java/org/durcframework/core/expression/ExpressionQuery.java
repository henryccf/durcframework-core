package org.durcframework.core.expression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.durcframework.core.SearchSupport;
import org.durcframework.core.expression.subexpression.ListExpression;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.springframework.util.StringUtils;

/**
 * 查询条件类
 * 
 * @author thc 2011-10-22
 */
public class ExpressionQuery {
	/** 起始索引 */
	private int start;
	/** 数据长度,即每页10条记录 */
	private int limit = 10;
	/** 排序字段 */
	private String sortname;
	/** 排序方式 */
	private String sortorder;
	// 是否查询全部
	private boolean isQueryAll;
	
	private Map<String, Object> paramMap = new HashMap<String, Object>();

	private List<ValueExpression> valueExprList = new ArrayList<ValueExpression>();
	private List<JoinExpression> joinExprList = new ArrayList<JoinExpression>();
	private List<ListExpression> listExprList = new ArrayList<ListExpression>();
	
	public static ExpressionQuery buildQueryAll(){
		ExpressionQuery query = new ExpressionQuery();
		query.isQueryAll = true;
		return query;
	}
	
	public void addAll(List<Expression> expressions){
		if(expressions != null){
			for (Expression expression : expressions) {
				this.add(expression);
			}
		}
	}
	
	/**
	 * 添加注解查询条件
	 * @param searchEntity
	 * @return
	 */
	public ExpressionQuery addAnnotionExpression(SearchSupport searchEntity) {
		List<Expression> expresList = ExpressionBuilder.buildExpressions(searchEntity);

		for (Expression express : expresList) {
			add(express);
		}
		
		return this;
	}
	
	/**
	 * 添加分页信息
	 */
	public ExpressionQuery addPaginationInfo(SearchSupport searchEntity){
		this.start = searchEntity.getStart();
		this.limit = searchEntity.getLimit();
		this.sortname = searchEntity.getSortname();
		this.sortorder = searchEntity.getSortorder();
		
		return this;
	}

	public ExpressionQuery addValueExpression(ValueExpression expression) {
		valueExprList.add(expression);
		return this;
	}

	public ExpressionQuery addJoinExpression(JoinExpression expression) {
		joinExprList.add(expression);
		return this;
	}

	public ExpressionQuery addListExpression(ListExpression expression) {
		listExprList.add(expression);
		return this;
	}

	public ExpressionQuery addParam(String name,Object value) {
		paramMap.put(name, value);
		return this;
	}


	public ExpressionQuery add(Expression expre) {
		expre.addToQuery(this);
		return this;
	}
	
	/**
	 * 是否查询全部
	 * @return
	 */
	public boolean getIsQueryAll() {
		return this.isQueryAll;
	}

	public void setIsQueryAll(boolean isQueryAll) {
		this.isQueryAll = isQueryAll;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setPageSize(int pageSize) {
		this.setLimit(pageSize);
	}

	public int getPageSize() {
		return this.getLimit();
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSortname() {
		return sortname;
	}

	public void setSortname(String sortname) {
		this.sortname = sortname;
	}

	public String getSortorder() {
		return sortorder;
	}
	
	public void setSortorder(String sortorder) {
		if(SqlContent.ASC.equalsIgnoreCase(sortorder) || SqlContent.DESC.equalsIgnoreCase(sortorder)) {
			this.sortorder = sortorder;
		}else{
			this.sortorder = SqlContent.ASC;
		}
	}
	
	public String getOrder() {
		String sortname = getSortname();
		String sortorder = getSortorder();
		
		if (StringUtils.hasText(sortname) && StringUtils.hasText(sortorder)) {
			return SqlContent.BLANK + sortname + SqlContent.BLANK + sortorder
					+ SqlContent.BLANK;
		} else if (StringUtils.hasText(sortname)) {
			return SqlContent.BLANK + sortname + SqlContent.BLANK;
		} else {
			return null;
		}
	}
	
	/**
	 * 返回第一条记录
	 * 
	 * @return
	 */
	public int getFirstResult() {
		return this.start;
	}

	public void setFirstResult(int firstResult) {
		this.start = firstResult;
	}

	public List<ValueExpression> getValueExprList() {
		return valueExprList;
	}

	public void setValueExprList(List<ValueExpression> valueExprList) {
		this.valueExprList = valueExprList;
	}

	public List<JoinExpression> getJoinExprList() {
		return joinExprList;
	}

	public void setJoinExprList(List<JoinExpression> joinExprList) {
		this.joinExprList = joinExprList;
	}

	public List<ListExpression> getListExprList() {
		return listExprList;
	}

	public void setListExprList(List<ListExpression> listExprList) {
		this.listExprList = listExprList;
	}

	public Map<String, Object> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, Object> paramMap) {
		this.paramMap = paramMap;
	}

}
