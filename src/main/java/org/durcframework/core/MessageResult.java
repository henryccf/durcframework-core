package org.durcframework.core;

import java.util.List;

public interface MessageResult {

	void setSuccess(boolean success);

	void setMsg(String msg);

	void setErrorMsg(String errorMsg);

	void setErrorMsgs(List<String> errorMsgs);
}
