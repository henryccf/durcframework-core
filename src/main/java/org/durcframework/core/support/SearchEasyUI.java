package org.durcframework.core.support;

import java.util.Map;

import org.durcframework.core.SearchSupport;

/**
 * jquery easyUI的datagrid查询类
 */
public class SearchEasyUI implements SearchSupport {

	// 当前第几页
	private int page = 1;
	// 每页记录数
	private int rows = 20;
	
	private String sort;
	private String order;
	
	/**
	 * 子类可覆盖此方法
	 * <pre>
	 * <code>
	 * private static Map<String, String> sortMap;
	
	static {
		sortMap = new HashMap<String, String>(2);
		// 排序字段映射,java类中的字段对应成数据库中的字段
		sortMap.put("addTime", "add_time");
		sortMap.put("lastLoginDate", "last_login_date");
	}
	
	protected Map<String, String> getSortMap() {
		return sortMap;
	}
	 * </code>
	 * </pre>
	 * @return
	 */
	protected Map<String,String> getSortMap(){
		return null;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
	

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String getSortname() {
		return buildSortname(this.sort);
	}
	
	// 构建真实的排序名称
	// 子类覆盖getSortMap()方法,并返回java字段与数据库中的字段名隐射关系
	private String buildSortname(String defSortname){
		Map<String, String> sortMap = this.getSortMap();
		
		if(sortMap != null) {
			String sortname = sortMap.get(defSortname);
			if(sortname != null){
				return sortname;
			}
		}
		
		return defSortname;
	}

	@Override
	public String getSortorder() {
		return this.order;
	}
	
	@Override
	public int getLimit() {
		return rows;
	}

	@Override
	public int getStart() {
		return (int) ((page - 1) * rows);
	}

}
