package org.durcframework.core;

import java.util.List;

public class DefaultMessageResult implements MessageResult {
	private boolean success;
	private String msg;
	private String errorMsg;
	private List<String> errorMsgs;

	public static DefaultMessageResult success(String msg) {
		DefaultMessageResult result = new DefaultMessageResult();
		result.setSuccess(true);
		result.setMsg(msg);
		return result;
	}

	public static DefaultMessageResult success() {
		DefaultMessageResult result = new DefaultMessageResult();
		result.setSuccess(true);
		return result;
	}

	public static DefaultMessageResult error(String errorMsg) {
		DefaultMessageResult result = new DefaultMessageResult();
		result.setSuccess(false);
		result.setErrorMsg(errorMsg);
		return result;
	}

	public static DefaultMessageResult error(String errorMsg, List<String> errors) {
		DefaultMessageResult result = error(errorMsg);
		result.setErrorMsgs(errors);
		return result;
	}

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	@Override
	public void setErrorMsg(String errorMsg) {
		this.success = false;
		this.errorMsg = errorMsg;
	}

	@Override
	public void setErrorMsgs(List<String> errorMsgs) {
		this.success = false;
		this.errorMsgs = errorMsgs;
	}

	public List<String> getErrorMsgs() {
		return this.errorMsgs;
	}

}
