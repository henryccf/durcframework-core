package org.durcframework.core;

import java.util.Map;

/**
 * 处理JSON对象的接口
 * 
 * @author hc.tang
 * 
 */
public interface JsonObjProcessor<Entity> {
	void process(Entity entity, Map<String,Object> jsonObject);
}
